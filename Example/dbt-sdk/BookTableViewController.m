//
//  BookTableViewController.m
//  DBT Test
//
//  Created by Brandon Trebitowski on 9/4/14.
//  Copyright (c) 2014 Treb Studios. All rights reserved.
//

#import "BookTableViewController.h"
#import "ChapterTableViewController.h"
#import "DBT.h"
#import "DBTVolume.h"
#import "DBTBook.h"

@interface BookTableViewController ()
@property(nonatomic,strong) NSArray *books;
@end

@implementation BookTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.title = self.volume.volumeName;
    
    [DBT getLibraryBookWithDamId:self.volume.damId
                  success:^(NSArray *books) {
                      self.books = books;
                      [self.tableView reloadData];
                  } failure:^(NSError *error) {
                      NSLog(@"Error: %@", error);
                  }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return self.books.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    // Configure the cell...
    DBTBook *book = self.books[indexPath.row];
    cell.textLabel.text = book.bookName;
    return cell;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"showDetail"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        DBTBook *book = self.books[indexPath.row];
        [[segue destinationViewController] setBook:book];
        [[segue destinationViewController] setVolume:self.volume];
    }
}

@end
