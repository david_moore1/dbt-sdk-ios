//
//  VerseViewController.m
//  DBT Test
//
//  Created by Brandon Trebitowski on 9/4/14.
//  Copyright (c) 2014 Treb Studios. All rights reserved.
//

#import "VerseViewController.h"
#import "DBTBook.h"
#import "DBT.h"
#import "DBTVerse.h"
#import "DBTMediaLocation.h"

@interface VerseViewController ()
@property(nonatomic, weak) IBOutlet UIWebView *webView;
@property(nonatomic, strong) NSArray *verses;
@end

@implementation VerseViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = [NSString stringWithFormat:@"%@: %@",self.book.bookName, self.chapter];
    // Do any additional setup after loading the view.
    NSNumber *chapterNumber = [NSNumber numberWithInteger:[self.chapter integerValue]];
    [DBT getTextVerseWithDamId:self.book.damId
                          book:self.book.bookId
                       chapter:chapterNumber
                    verseStart:nil
                      verseEnd:nil
                       success:^(NSArray *verses) {
                           self.verses = verses;
                           [self renderText];
                       } failure:^(NSError *error) {
                           NSLog(@"Error: %@", error);
                       }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void) renderText {
    
    NSMutableString *htmlString = [NSMutableString string];
    
    for(DBTVerse *verse in self.verses) {
        [htmlString appendFormat:@"<strong>%@</strong> %@",verse.verseId, verse.verseText];
    }
    
    [self.webView loadHTMLString:htmlString baseURL:nil];
}

@end
